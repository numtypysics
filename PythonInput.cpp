
/*
 * Python Input Module for NumptyPhysics
 * Copyright (c) 2009 Thomas Perl <thpinfo.com>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 */

#include "PythonInput.h"
#include "Multitouch.h"

#ifdef HAVE_PYTHON

#include <Python.h>
#include "SDL/SDL.h"
#include <SDL/SDL_mixer.h>

#include <iostream>
#include <string>


int PythonInput::m_width = 0;

static char* soundFiles[] = {
    "scraping.ogg",
    "blue.ogg",
    "drag.ogg"
};

const int CHANNELS = 8;

class SoundManager
{
    public:
        enum SoundType {
            SND_DRAW,
            SND_DRAG,
            SND_DELETE,
            SND_MAX
        };

        SoundManager()
        {
            Mix_AllocateChannels(CHANNELS);
            Mix_Chunk* tmp;
            for (int i=0; i<SND_MAX; i++) {
                tmp = Mix_LoadWAV(soundFiles[i]);
                if (tmp == NULL) {
                    throw std::string("Cannot open sound file: ") + std::string(soundFiles[i]);
                }
                sounds[i] = tmp;
            }
        }

        ~SoundManager()
        {
            for (int i=0; i<SND_MAX; i++) {
                Mix_FreeChunk(sounds[i]);
            }
        }

        void
        playSound(SoundType s, int id, float x=1., bool single=false)
        {
            std::cerr << "x = " << x << std::endl;
            Mix_PlayChannel(id%CHANNELS, sounds[s], single?0:10);
            updatePosition(id, x);
        }

        void
        updatePosition(int id, float x)
        {
            Mix_SetPanning(id%CHANNELS, 254*(1.-x), 254*(x));
        }

        void
        stopSound(int id)
        {
            Mix_FadeOutChannel(id%CHANNELS, 100);
        }

    private:
        Mix_Chunk* sounds[SND_MAX];
};

PyObject* PythonInput::create_module()
{
    static PyMethodDef NumptyMethods[] = {
        {"post_event", post_event, METH_O, "Post a new mouse event"},
        {NULL, NULL, 0, NULL} /* Sentinel */
    };

    PyObject* module;
    module = Py_InitModule("numptyphysics", NumptyMethods);

    PyModule_AddIntConstant(module, "START_STROKE", SDL_NP_START_STROKE);
    PyModule_AddIntConstant(module, "APPEND_STROKE", SDL_NP_APPEND_STROKE);
    PyModule_AddIntConstant(module, "FINISH_STROKE", SDL_NP_FINISH_STROKE);
    PyModule_AddIntConstant(module, "START_ROPE", SDL_NP_START_ROPE);
    PyModule_AddIntConstant(module, "APPEND_ROPE", SDL_NP_APPEND_ROPE);
    PyModule_AddIntConstant(module, "FINISH_ROPE", SDL_NP_FINISH_ROPE);
    PyModule_AddIntConstant(module, "PREVIEW_CURSOR", SDL_NP_PREVIEW_CURSOR);
    PyModule_AddIntConstant(module, "CANCEL_DRAW", SDL_NP_CANCEL_DRAW);
    PyModule_AddIntConstant(module, "START_DRAG", SDL_NP_START_DRAG);
    PyModule_AddIntConstant(module, "DRAG", SDL_NP_DRAG);
    PyModule_AddIntConstant(module, "END_DRAG", SDL_NP_END_DRAG);
    PyModule_AddIntConstant(module, "PAN", SDL_NP_PAN);
    PyModule_AddIntConstant(module, "ZOOM", SDL_NP_ZOOM);
    PyModule_AddIntConstant(module, "DELETE", SDL_NP_DELETE);

    PyModule_AddIntConstant(module, "WIDTH", PythonInput::m_width);
    PyModule_AddIntConstant(module, "HEIGHT", m_height);
    PyModule_AddIntConstant(module, "MAX_CURSORS", MT_MAX_CURSORS);

    return module;
}

PyObject* PythonInput::post_event(PyObject* self, PyObject* event)
{
    static SoundManager sound;

    int xpos = -1, ypos = -1;
    int type = -1;
    int cursor_id = -1;
    PyObject *o = NULL;
    SDL_Event e = {0};

    assert(self == NULL);

    /* Get X position */
    o = PyObject_GetAttrString(event, "x");
    if (PyInt_Check(o)) {
        xpos = (int)PyInt_AsLong(o);
    } else {
        fprintf(stderr, "x not a number: ");
        PyObject_Print(o, stderr, 0);
        fprintf(stderr, "\n");
        exit(1);
    }
    Py_DECREF(o);

    /* Get Y position */
    o = PyObject_GetAttrString(event, "y");
    if (PyInt_Check(o)) {
        ypos = (int)PyInt_AsLong(o);
    } else {
        fprintf(stderr, "y not a number: ");
        PyObject_Print(o, stderr, 0);
        fprintf(stderr, "\n");
        exit(1);
    }
    Py_DECREF(o);

    /* Get type */
    o = PyObject_GetAttrString(event, "event_type");
    if (PyInt_Check(o)) {
        type = (int)PyInt_AsLong(o);
    } else {
        fprintf(stderr, "event_type is not an int: ");
        PyObject_Print(o, stderr, 0);
        fputc('\n', stderr);
        exit(1);
    }
    Py_DECREF(o);

    /* Get cursor_id */
    o = PyObject_GetAttrString(event, "cursor_id");
    if (PyInt_Check(o)) {
        cursor_id = (int)PyInt_AsLong(o);
    } else {
        fprintf(stderr, "cursor_id is not an int: ");
        PyObject_Print(o, stderr, 0);
        fputc('\n', stderr);
        exit(1);
    }
    Py_DECREF(o);

    if (type == SDL_NP_START_STROKE) {
        sound.playSound(sound.SND_DRAW, cursor_id, (float)xpos/(float)PythonInput::m_width);
        queueStartStrokeEvent(cursor_id, xpos, ypos);
    } else if (type == SDL_NP_FINISH_STROKE) {
        sound.stopSound(cursor_id);
        queueFinishStrokeEvent(cursor_id, xpos, ypos);
    } else if (type == SDL_NP_APPEND_STROKE) {
        sound.updatePosition(cursor_id, (float)xpos/(float)PythonInput::m_width);
        queueAppendStrokeEvent(cursor_id, xpos, ypos);
    } else if (type == SDL_NP_START_DRAG) {
        sound.playSound(sound.SND_DRAG, cursor_id, (float)xpos/(float)PythonInput::m_width);
        queueStartDragEvent(cursor_id, xpos, ypos);
    } else if (type == SDL_NP_END_DRAG) {
        sound.stopSound(cursor_id);
        queueEndDragEvent(cursor_id, xpos, ypos);
    } else if (type == SDL_NP_DRAG) {
        sound.updatePosition(cursor_id, (float)xpos/(float)PythonInput::m_width);
        queueDragEvent(cursor_id, xpos, ypos);
    } else if (type == SDL_NP_PREVIEW_CURSOR) {
        queuePreviewCursorEvent(cursor_id, xpos, ypos);
    } else if (type == SDL_NP_DELETE) {
        sound.playSound(sound.SND_DELETE, cursor_id, (float)xpos/(float)PythonInput::m_width, true);
        queueDeleteEvent(xpos, ypos);
    } else if (type == SDL_NP_CANCEL_DRAW) {
        queueCancelDrawEvent(cursor_id);
    } else {
        fprintf(stderr, "Warning: unknown event type %d.\n", type);
        exit(1);
    }

    //return PyErr_Format(PyExc_TypeError, "This function needs a bot class to work");
    Py_RETURN_NONE;
}

#endif /* HAVE_PYTHON */
