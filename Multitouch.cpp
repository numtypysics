#include "Multitouch.h"
#include <SDL/SDL.h>
#include <stdio.h>

#define allocate(x) ((x*)malloc(sizeof(x)))

void queueDrawEvent(int type, int cursor_id, int x, int y);
void queueDraggingEvent(int type, int cursor_id, int x, int y);

void queueStartStrokeEvent(int cursor_id, int x, int y)
{
    queueDrawEvent(SDL_NP_START_STROKE, cursor_id, x, y);
}

void queueAppendStrokeEvent(int cursor_id, int x, int y)
{
    queueDrawEvent(SDL_NP_APPEND_STROKE, cursor_id, x, y);
}

void queueFinishStrokeEvent(int cursor_id, int x, int y)
{
    queueDrawEvent(SDL_NP_FINISH_STROKE, cursor_id, x, y);
}

void queueCancelDrawEvent(int cursor_id)
{
    queueCursorEvent(SDL_NP_CANCEL_DRAW, cursor_id);
}

void queuePreviewCursorEvent(int cursor_id, int x, int y)
{
    queueDrawEvent(SDL_NP_PREVIEW_CURSOR, cursor_id, x, y);
}

void queueStartRopeEvent(int cursor_id, int x, int y)
{
    queueDrawEvent(SDL_NP_START_ROPE, cursor_id, x, y);
}

void queueAppendRopeEvent(int cursor_id, int x, int y)
{
    queueDrawEvent(SDL_NP_APPEND_ROPE, cursor_id, x, y);
}

void queueFinishRopeEvent(int cursor_id, int x, int y)
{
    queueDrawEvent(SDL_NP_FINISH_ROPE, cursor_id, x, y);
}

void queueCursorEvent(int type, int cursor_id)
{
    CursorEvent* ce = allocate(CursorEvent);
    ce->cursor_id = cursor_id;

    queueUserEvent(type, ce, 0);
}

void queueDrawEvent(int type, int cursor_id, int x, int y)
{
    DrawEvent* de = allocate(DrawEvent);
    de->cursor_id = cursor_id;
    de->x = x;
    de->y = y;

    queueUserEvent(type, de, 0);
}

void queueStartDragEvent(int cursor_id, int x, int y)
{
    queueDraggingEvent(SDL_NP_START_DRAG, cursor_id, x, y);
}

void queueDragEvent(int cursor_id, int x, int y)
{
    queueDraggingEvent(SDL_NP_DRAG, cursor_id, x, y);
}

void queueEndDragEvent(int cursor_id, int x, int y)
{
    queueDraggingEvent(SDL_NP_END_DRAG, cursor_id, x, y);
}

void queueDraggingEvent(int type, int cursor_id, int x, int y)
{
    DragEvent* de = allocate(DragEvent);
    de->cursor_id = cursor_id;
    de->x = x;
    de->y = y;

    queueUserEvent(type, de, 0);
}

void queuePanEvent(int xdiff, int ydiff)
{
    PanEvent* pe = allocate(PanEvent);
    pe->xdiff = xdiff;
    pe->ydiff = ydiff;

    queueUserEvent(SDL_NP_PAN, pe, 0);
}

void queueZoomEvent(int x, int y, float zoomfactor)
{
    ZoomEvent* ze = allocate(ZoomEvent);
    ze->x = x;
    ze->y = y;
    ze->zoomfactor = zoomfactor;

    queueUserEvent(SDL_NP_ZOOM, ze, 0);
}

void queueDeleteEvent(int x, int y)
{
    DeleteEvent* de = allocate(DeleteEvent);
    de->x = x;
    de->y = y;

    queueUserEvent(SDL_NP_DELETE, de, 0);
}

void queueRestartLevelEvent()
{
    queueUserEvent(SDL_NP_RESTART_LEVEL, 0, 0);
}

void queueNextLevelEvent()
{
    queueUserEvent(SDL_NP_NEXT_LEVEL, 0, 0);
}

void queuePreviousLevelEvent()
{
    queueUserEvent(SDL_NP_PREV_LEVEL, 0, 0);
}

void queueToggleMenuEvent()
{
    queueUserEvent(SDL_NP_TOGGLE_MENU, 0, 0);
}

void queueShowMenuEvent()
{
    queueUserEvent(SDL_NP_SHOW_MENU, 0, 0);
}
 
void queueHideMenuEvent()
{
    queueUserEvent(SDL_NP_HIDE_MENU, 0, 0);
}

void queueTogglePauseEvent()
{

    queueUserEvent(SDL_NP_TOGGLE_PAUSE, 0, 0);
}

void queuePauseEvent()
{
    queueUserEvent(SDL_NP_PAUSE, 0, 0);
}

void queueResumeEvent()
{
    queueUserEvent(SDL_NP_RESUME, 0, 0);
}

void queueSaveEvent()
{
    queueUserEvent(SDL_NP_SAVE, 0, 0);
}

void queueToggleEditorEvent()
{
    queueUserEvent(SDL_NP_TOGGLE_EDITOR, 0, 0);
}

void queueReplayEvent()
{
    queueUserEvent(SDL_NP_REPLAY, 0, 0);
}

void queueUserEvent(int type, void *data1, void *data2)
{
    SDL_Event event;
    event.type = type;
    event.user.data1 = data1;
    event.user.data2 = data2;

    fprintf(stderr, "Posting event: %d (%p, %p)\n", type, data1, data2);

    if (SDL_PushEvent(&event) != 0)
    {
        fprintf(stderr, "could not push SDL event onto queue");
    }
}
