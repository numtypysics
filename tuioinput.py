
#
# Python Input Module for NumptyPhysics
# Copyright (c) 2009 Thomas Perl <thpinfo.com>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
#


import sys
import threading
import time
import collections
import os.path

# Create the file "use-ipod" if you want to use an iPod Touch
# and its OSCemote application for TUIO input (rm it if you don't)
use_ipod = os.path.exists('use-ipod')

# Create the file "use-mtmini" if you want to show cursor previews
use_mtmini = os.path.exists('use-mtmini')

# Create the file "use-justdraw" if you don't want dragging+deleting
use_justdraw = os.path.exists('use-justdraw')

CURSOR_ACTIVATE_DELAY = .01
CURSOR_UPGRADE_DELAY = .5
CURSOR_STOP_DELAY = .5

CURSOR_NEIGHBOR_DISTANCE = .1

try:
    import numptyphysics
except ImportError, ioe:
    print >>sys.stderr, """
    This module can only be loaded from within NumptyPhysics.
    """
    sys.exit(1)

try:
    import tuio
except ImportError, ioe:
    print >>sys.stderr, """
    You have to install PyTUIO and copy it into your $PYTHONPATH.
    You can grab a tarball from: http://code.google.com/p/pytuio/
    """
    sys.exit(2)

CURSOR_NEIGHBOR_DISTANCE *= numptyphysics.HEIGHT

class NumpytTuioCursor(object):
    def __init__(self, id, x, y):
        self.id = id
        self.x = x
        self.y = y
        self.time = time.time()

class NumptyCursor(object):
    DRAW, DRAG, DELETE = range(3)

    def __init__(self, tuio_id, x, y, on_activate, on_deactivate, on_move, on_upgrade):
        self.id = None
        self.x = x
        self.y = y
        self.mode = self.DRAW
        self.tuio_ids = {tuio_id: time.time()}

        self.activated = False
        self.deactivated = False
        self.moved = False
        self.upgraded = False

        self.on_activate = on_activate
        self.on_deactivate = on_deactivate
        self.on_move = on_move
        self.on_upgrade = on_upgrade

        self.activate_at = time.time()+CURSOR_ACTIVATE_DELAY
        self.upgrade_at = time.time()+CURSOR_UPGRADE_DELAY
        self.deactivate_at = 0
        print 'new numpty cursor for', tuio_id
        self.heartbeat()

    def heartbeat(self):
        self.deactivate_at = time.time()+CURSOR_STOP_DELAY

    def is_near(self, x, y):
        return ((x-self.x)**2 + (y-self.y)**2) < CURSOR_NEIGHBOR_DISTANCE**2

    def move(self, x, y):
        if self.activated and not self.deactivated:
            print 'moved', self.id, 'to', x, '/', y, '(which is a', self.mode, ')'
        if self.x != x or self.y != y:
            self.x = x
            self.y = y
            self.moved = True

    def want_new_cursors(self):
        if self.mode == self.DRAW:
            return len(self.tuio_ids) < 1
        elif self.mode == self.DRAG:
            return len(self.tuio_ids) < 2
        elif self.mode == self.DELETE:
            return len(self.tuio_ids) < 3
        return False

    def seen_tuio_id(self, id, x, y):
        if self.deactivated:
            # If this cursor is gone, it's gone
            return False

        if not self.upgraded and self.is_near(x, y) and id not in self.tuio_ids and not use_justdraw:
            # this cursor it not yet upgraded; the tuio id is near
            if self.mode == self.DRAW:
                print 'absorbing and setting to drag', id
                self.mode = self.DRAG
            elif self.mode == self.DRAG:
                print 'absorbing and setting to delete', id
                self.mode = self.DELETE
            else:
                return False

            self.tuio_ids[id] = time.time()
            self.heartbeat()
            return True

        if self.upgraded and self.want_new_cursors() and self.is_near(x, y) and id not in self.tuio_ids:
            # i can take more cursors, so absorb this
            self.tuio_ids[id] = time.time()
            if id == min(self.tuio_ids) and self.id:
                self.move(x, y)
            self.heartbeat()
            return True
        elif id in self.tuio_ids:
            self.tuio_ids[id] = time.time()
            if id == min(self.tuio_ids) and self.id:
                self.move(x, y)
            self.heartbeat()
            return True

        return False

    def activate(self):
        self.activated = True
        self.on_activate(self)

    def upgrade(self):
        self.upgraded = True
        self.on_upgrade(self)
        if self.mode == self.DELETE:
            self.deactivate()

    def deactivate(self):
        self.deactivated = True
        if self.activated:
            self.on_deactivate(self)
    
    def movement(self):
        self.on_move(self)
        self.moved = False

    def process(self):
        if time.time() > self.deactivate_at and not self.deactivated:
            self.deactivate()
        elif time.time() > self.upgrade_at and not self.upgraded and not self.deactivated:
            self.upgrade()
        elif time.time() > self.activate_at and not self.activated and not self.upgraded and not self.deactivated:
            self.activate()
        elif self.activated and self.moved:
            self.movement()
        else:
            self.expire()

    def expire(self):
        for id, t in self.tuio_ids.items():
            if t+.5*CURSOR_STOP_DELAY < time.time():
                del self.tuio_ids[id]


class CursorTracker(object):
    def __init__(self):
        self.cursors = []
        self._freeslots = collections.deque(range(numptyphysics.MAX_CURSORS))
        self._highest_id = -1
        self.events = collections.deque()

    def activate_cursor(self, cursor):
        cursor.id = self.grabslot()
        if cursor.mode == cursor.DRAW:
            self.push_event(cursor, numptyphysics.START_STROKE)
        elif cursor.mode == cursor.DRAG:
            self.push_event(cursor, numptyphysics.START_DRAG)
        elif cursor.mode == cursor.DELETE:
            self.push_event(cursor, numptyphysics.DELETE)

    def deactivate_cursor(self, cursor):
        if cursor.mode == cursor.DRAW:
            self.push_event(cursor, numptyphysics.FINISH_STROKE)
        elif cursor.mode == cursor.DRAG:
            self.push_event(cursor, numptyphysics.END_DRAG)
        self.freeslot(cursor.id)

    def cursor_movement(self, cursor):
        if cursor.mode == cursor.DRAW:
            self.push_event(cursor, numptyphysics.APPEND_STROKE)
        elif cursor.mode == cursor.DRAG:
            self.push_event(cursor, numptyphysics.DRAG)

    def upgrade_cursor(self, cursor):
        if cursor.mode == cursor.DRAW:
            # "draw" cursors do not need to be upgraded
            return

        # cancel the in-progress draw event + start "real" event
        self.push_event(cursor, numptyphysics.CANCEL_DRAW)
        if cursor.mode == cursor.DRAG:
            self.push_event(cursor, numptyphysics.START_DRAG)
        elif cursor.mode == cursor.DELETE:
            self.push_event(cursor, numptyphysics.DELETE)

    def update(self, cursors):
        new_cursors = []
        for cursor in cursors:
            id, x, y = cursor.sessionid, cursor.xpos, cursor.ypos
            if x == 0 and y == 0:
                continue
            if use_ipod:
                x, y = y, 1.-x
            x, y = self.convert_coords(x, y)
            if use_mtmini:
                self.push_preview_event(id, x, y)

            absorbed = False
            for c in self.cursors:
                if c.seen_tuio_id(id, x, y):
                    absorbed = True
                    break

            if not absorbed and id > self._highest_id:
                new_cursors.append((id, x, y))

            if id > self._highest_id:
                self._highest_id = id

        for id, x, y in new_cursors:
            self.cursors.append(NumptyCursor(id, x, y, self.activate_cursor, self.deactivate_cursor, self.cursor_movement, self.upgrade_cursor))

        self.idle()

    def idle(self):
        for cursor in self.cursors:
            cursor.process()

        self.cursors = [cursor for cursor in self.cursors if not cursor.deactivated]

    def grabslot(self):
        try:
            return self._freeslots.pop()
        except IndexError:
            return None

    def freeslot(self, slot):
        self._freeslots.appendleft(slot)

    def push_event(self, cursor, type_):
        if cursor.id:
            print 'pushing event type %d for cursor %s' % (type_, repr(cursor.id))
            self.events.appendleft(InputEvent(cursor.x, cursor.y, type_, cursor.id))

    def push_preview_event(self, id, x, y):
        self.events.appendleft(InputEvent(x, y, numptyphysics.PREVIEW_CURSOR, id))

    def convert_coords(self, x, y):
        return (int(x*numptyphysics.WIDTH), int(y*numptyphysics.HEIGHT))
    
    def has_events(self):
        return len(self.events) > 0

    def get_events(self):
        try:
            while True:
                yield self.events.pop()
        except IndexError, ie:
            raise StopIteration()

class InputEvent(object):
    def __init__(self, x, y, event_type, cursor_id=0):
        self.x = x
        self.y = y
        self.event_type = event_type
        self.cursor_id = cursor_id

class C(threading.Thread):
    def run(self):
        pointlog = open('pointlog-'+str(int(time.time()))+'.np', 'w')
        tracking = tuio.Tracking('')
        tracker = CursorTracker()
        oldcount = False
        x, y = None, None
        while True:
            while tracking.update():
                # read the socket empty
                pass

            tracker.update(tracking.cursors())

            for event in tracker.get_events():
                pointlog.write('/'.join((str(event.x), str(event.y), str(int(time.time())))))
                pointlog.write('\n')
                numptyphysics.post_event(event)

            while not tracking.update() and not tracker.has_events():
                # wait for something to happen
                tracker.idle()
                time.sleep(1./30.)

C().start()


