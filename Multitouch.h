#include <SDL/SDL.h>

#define MT_MAX_CURSORS 10

/* we don't want to break any unrelated code */
#define m_createStroke m_createStrokes[0]
#define m_moveStroke   m_moveStrokes[0]

struct t_draw_event {
    int cursor_id;
    int x;
    int y;
};

typedef struct t_draw_event DrawEvent;


struct t_drag_event {
    int cursor_id;
    int x;
    int y;
};

typedef struct t_drag_event DragEvent;


struct t_pan_event {
    int xdiff;
    int ydiff;
};

typedef struct t_pan_event PanEvent;


struct t_zoom_event {
    int x;
    int y;
    float zoomfactor;
};

typedef struct t_zoom_event ZoomEvent;


struct t_delete_event {
    int x;
    int y;
};

typedef struct t_delete_event DeleteEvent;


struct t_cursor_event {
    int cursor_id;
};

typedef struct t_cursor_event CursorEvent;


enum 
{
    /* SDL_Events with one of the following have a DrawEvent as data1 */
    SDL_NP_START_STROKE = SDL_USEREVENT+1,
    SDL_NP_APPEND_STROKE,
    SDL_NP_FINISH_STROKE,

    SDL_NP_START_ROPE,
    SDL_NP_APPEND_ROPE,
    SDL_NP_FINISH_ROPE,

    SDL_NP_PREVIEW_CURSOR,

    /* SDL_Event with one of the following types have a CursorEvent as data1 */
    SDL_NP_CANCEL_DRAW,

    /* SDL_Events with one of the following types have a DragEvent as data1 */
    SDL_NP_START_DRAG,
    SDL_NP_DRAG,
    SDL_NP_END_DRAG,

    /* SDL_Events with one of the following types have a PanEvent as data1 */
    SDL_NP_PAN,

    /* SDL_Events with one of the following types have a ZoomEvent as data1 */
    SDL_NP_ZOOM,

    /* SDL_Events with one of the following types have a DeletEvent as data1 */
    SDL_NP_DELETE,

    /* SDL_Events with one of the following types don't have any user data */
    SDL_NP_RESTART_LEVEL,
    SDL_NP_NEXT_LEVEL,
    SDL_NP_PREV_LEVEL,
    SDL_NP_TOGGLE_MENU,
    SDL_NP_SHOW_MENU,
    SDL_NP_HIDE_MENU,
    SDL_NP_TOGGLE_PAUSE,
    SDL_NP_PAUSE,
    SDL_NP_RESUME,
    SDL_NP_SAVE,
    SDL_NP_TOGGLE_EDITOR,
    SDL_NP_REPLAY


};

void queueStartStrokeEvent(int cursor_id, int x, int y);
void queueAppendStrokeEvent(int cursor_id, int x, int y);
void queueFinishStrokeEvent(int cursor_id, int x, int y);
void queueCancelDrawEvent(int cursor_id);
void queuePreviewCursorEvent(int cursor_id, int x, int y);
void queueCursorEvent(int type, int cursor_id);
void queueStartRopeEvent(int cursor_id, int x, int y);
void queueAppendRopeEvent(int cursor_id, int x, int y);
void queueFinishRopeEvent(int cursor_id, int x, int y);
void queueStartDragEvent(int cursor_id, int x, int y);
void queueDragEvent(int cursor_id, int x, int y);
void queueEndDragEvent(int cursor_id, int x, int y);
void queuePanEvent(int xdiff, int ydiff);
void queueZoomEvent(int x, int y, float zoomfactor);
void queueDeleteEvent(int x, int y);
void queueRestartLevelEvent();
void queueNextLevelEvent();
void queuePreviousLevelEvent();
void queueToggleMenuEvent();
void queueShowMenuEvent();
void queueHideMenuEvent();
void queueTogglePauseEvent();
void queuePauseEvent();
void queueResumeEvent();
void queueSaveEvent();
void queueToggleEditorEvent();
void queueReplayEvent();
void queueUserEvent(int type, void *data1, void *data2);
