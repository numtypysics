
/*
 * Python Input Module for NumptyPhysics
 * Copyright (c) 2009 Thomas Perl <thpinfo.com>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 */

#ifndef PYTHONINPUT_H
#define PYTHONINPUT_H

#ifdef HAVE_PYTHON

#include <Python.h>
#include "SDL/SDL.h"

class PythonInput {
    private:
        PyThreadState* m_python_threadstate;
        static int m_width;
        int m_height;

        PyObject* create_module();
        static PyObject* post_event(PyObject* self, PyObject *event);
    public:
        PythonInput(int width, int height)
            : m_height(height)
        {
            m_width = width;
            PyObject* numpty_module;
            PyObject* input_module;

            /* Initialize the Python Interpreter */
            setenv("PYTHONPATH", ".", 1);
            PyEval_InitThreads();
            Py_Initialize();

            /* Add the glue module and load tuioinput */
            numpty_module = create_module();
            input_module = PyImport_ImportModule("tuioinput");
            if (input_module == NULL) {
                PyErr_Print();
            }
            Py_DECREF(input_module);
            Py_DECREF(numpty_module);

            m_python_threadstate = PyEval_SaveThread();
        }

        ~PythonInput()
        {
            PyEval_RestoreThread(m_python_threadstate);
            /* Could/should call Py_Finalize() here, but crashes */
        }
};

#endif /* HAVE_PYTHON */

#endif
