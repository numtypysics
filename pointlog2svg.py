#!/usr/bin/python
import sys

ORIGINAL, TABLE = (800, 480), (1024, 768)
STYLE = 'fill:#000000;opacity:.2;'

cx = lambda x: int(x)*ORIGINAL[0]/TABLE[0]
cy = lambda y: int(y)*ORIGINAL[1]/TABLE[1]

print '<?xml version="1.0" encoding="UTF-8" standalone="no"?>'
print ('<svg xmlns="http://www.w3.org/2000/svg" width="%d" '+
       'height="%d" version="1.1">') % ORIGINAL

for line in sys.stdin:
    line = line.strip().split('/')
    if len(line) != 4:
        continue
    x, y, curid, timestamp = line
    print ('<path style="%s" d="m %d,%d a 5.5,5.5 0 1 1'+
           '-11,0 5.5,5.5 0 1 1 11,0 z"/>') % (STYLE, cx(x), cy(y))

print '</svg>'

