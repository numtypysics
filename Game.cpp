/*
 * This file is part of NumptyPhysics
 * Copyright (C) 2008 Tim Edmonds
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 */

#include "Common.h"
#include "Array.h"
#include "Config.h"
#include "Game.h"
#include "Overlay.h"
#include "Path.h"
#include "Canvas.h"
#include "Font.h"
#include "Levels.h"
#include "Http.h"
#include "Os.h"
#include "Scene.h"

#include "Multitouch.h"

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>

#include <cstdio>
#include <iostream>
#include <sstream>
#include <fstream>
#include <memory.h>
#include <errno.h>
#include <sys/stat.h>

using namespace std;

unsigned char levelbuf[64*1024];



struct DemoEntry {
  DemoEntry( int _t, int _o, SDL_Event& _e ) : t(_t), o(_o), e(_e) {}
  int t,o;
  SDL_Event e;
};

class DemoLog : public Array<DemoEntry>
{
public:
  std::string asString( int i )
  {
    if ( i < size() ) {
      DemoEntry& e = at(i);
      stringstream s;
      s << "E:" << e.t << " ";
      switch( e.e.type ) {
      case SDL_KEYDOWN:
	s << "K" << e.e.key.keysym.sym;
	break;
      case SDL_KEYUP:
	s << "k" << e.e.key.keysym.sym;
	break;
      case SDL_MOUSEBUTTONDOWN:
	s << "B" << '0'+e.e.button.button;
	s << "," << e.e.button.x << "," << e.e.button.y;
	break;
      case SDL_MOUSEBUTTONUP:
	s << "b" << '0'+e.e.button.button;
	s << "," << e.e.button.x << "," << e.e.button.y;
	break;
      case SDL_MOUSEMOTION:
	s << "M" << e.e.button.x << "," << e.e.button.y;
	break;
      default:
	return std::string();
      }
      return s.str();
    }
    return std::string();
  }

  void append( int tick, int offset, SDL_Event& ev ) 
  {
    Array<DemoEntry>::append( DemoEntry( tick, offset, ev ) );
  }

  void appendFromString( const std::string& str ) 
  {
    const char *s = str.c_str();
    int t, o, v1, v2, v3;
    char c;
    SDL_Event ev = {0};
    ev.type = 0xff;
    if ( sscanf(s,"E:%d %c%d",&t,&c,&v1)==3 ) { //1 arg
      switch ( c ) {
      case 'K': ev.type = SDL_KEYDOWN; break;
      case 'k': ev.type = SDL_KEYUP; break;
      }
      ev.key.keysym.sym = (SDLKey)v1;
    } else if ( sscanf(s,"E:%d %c%d,%d",&t,&c,&v1,&v2)==4 ) { //2 args
      switch ( c ) {
      case 'M': ev.type = SDL_MOUSEMOTION; break;
      }
      ev.button.x = v1;
      ev.button.y = v2;
    } else if ( sscanf(s,"E:%d %c%d,%d,%d",&t,&c,&v1,&v2,&v3)==5 ) { //3 args
      switch ( c ) {
      case 'B': ev.type = SDL_MOUSEBUTTONDOWN; break;
      case 'b': ev.type = SDL_MOUSEBUTTONUP; break;
      }
      ev.button.button = v1;
      ev.button.x = v2;
      ev.button.y = v3;
    }
    if ( ev.type != 0xff ) {
      append( t, o, ev );
    }
  }
};

class DemoRecorder
{
public:

  void start() 
  {
    m_running = true;
    m_log.empty();
    m_log.capacity(512);
    m_lastTick = 0;
    m_lastTickTime = SDL_GetTicks();
    m_pressed = 0;
  }

  void stop()  
  { 
    printf("stop recording: %d events:\n", m_log.size());
    for ( int i=0; i<m_log.size(); i++ ) {
      std::string e = m_log.asString(i);
      if ( e.length() > 0 ) {
	printf("  %s\n",e.c_str());
      }
    }
    m_running = false; 
  }

  void tick() 
  {
    if ( m_running ) {
      m_lastTick++;
      m_lastTickTime = SDL_GetTicks();
    }
  }

  void record( SDL_Event& ev )
  {
    if ( m_running ) {
      switch( ev.type ) {      
      case SDL_MOUSEBUTTONDOWN: 
	m_pressed |= 1<<ev.button.button;
	break;
      case SDL_MOUSEBUTTONUP: 
	m_pressed &= ~(1<<ev.button.button);
	break;
      case SDL_MOUSEMOTION:
	if ( m_pressed == 0 ) {
	  return;
	}
      }
      m_log.append( m_lastTick, SDL_GetTicks()-m_lastTickTime, ev );
    }
  }
  
  DemoLog& getLog() { return m_log; }

private:
  bool          m_running;
  DemoLog       m_log;
  int 		m_lastTick;
  int 		m_lastTickTime;
  int           m_pressed;
};


class DemoPlayer
{
public:

  void start( const DemoLog* log ) 
  {
    m_playing = true;
    m_log = log;
    m_index = 0;
    m_lastTick = 0;
    printf("start playback: %d events\n",m_log->size());
  }

  bool isRunning() { return m_playing; }

  void stop()  
  { 
    m_playing = false; 
    m_log = NULL;
  }

  void tick() 
  {
    if ( m_playing ) {
      m_lastTick++;
    }
  }

  bool fetchEvent( SDL_Event& ev )
  {
    if ( m_playing ) {
      if ( m_index < m_log->size()
	   && m_log->at(m_index).t <= m_lastTick ) {
	printf("demo event at t=%d\n",m_lastTick);
	ev = m_log->at(m_index).e;
	m_index++;
	return true;
      }
    }
    return false;
  }
  
private:
  bool           m_playing;
  const DemoLog* m_log;
  int            m_index;
  int  		 m_lastTick;
};


class CollectionSelector : public ListProvider
{
  Overlay* m_list;
public:
  CollectionSelector( GameControl& game )
  {
    m_list = createListOverlay( game, this );
  }
  Overlay* overlay() { return m_list; }

  virtual int     countItems() {
    return 58;
  }
  virtual Canvas* provideItem( int i, Canvas* old ) {
    delete old;
    char buf[18];
    sprintf(buf,"%d. Item",i);
    Canvas* c = new Canvas( 100, 32 );
    c->setBackground(0xffffff);
    c->clear();
    Font::headingFont()->drawLeft( c, Vec2(3,3), buf, i<<3 );
    return c;
  }
  virtual void    releaseItem( Canvas* old ) {
    delete old;
  }
  virtual void    onSelection( int i, int ix, int iy ) { 
    printf("Selected: %d (%d,%d)\n",i,ix,iy);
  }
};


struct GameStats
{
  int startTime;
  int endTime;
  int strokeCount;
  int pausedStrokes;
  int undoCount;
  void reset() {
    startTime = SDL_GetTicks();
    strokeCount = 0;
    pausedStrokes = 0;
    undoCount = 0;
  }
};

class Game : public GameControl, public Widget
{
  Scene   	    m_scene;
  Stroke  	   *m_createStrokes[MT_MAX_CURSORS];
  Stroke           *m_moveStrokes[MT_MAX_CURSORS];
  Stroke           *m_ropeParts[MT_MAX_CURSORS][MAX_ROPE_PARTS];
  int               m_numRopeLength[MT_MAX_CURSORS];
  int               m_numRopeParts[MT_MAX_CURSORS];
  Array<Overlay*>   m_overlays;
  Window            m_window;
  Overlay          *m_pauseOverlay;
  Overlay          *m_editOverlay;
  Overlay          *m_completedOverlay;
  Overlay          *m_levelnameOverlay;
  int               m_levelnameHideTime;
  //  DemoOverlay       m_demoOverlay;
  DemoRecorder      m_recorder;
  DemoPlayer        m_player;
  CollectionSelector m_cselector;
  Os               *m_os;
  GameStats         m_stats;
  bool              m_isCompleted;
  bool              m_paused;
public:
  Game( Levels* levels, int width, int height, bool fullscreen )
  : m_window(width,height,"Numpty Physics","NPhysics", fullscreen),
    m_pauseOverlay( NULL ),
    m_editOverlay( NULL ),
    m_completedOverlay( NULL ),
    m_levelnameOverlay( NULL ),
    m_levelnameHideTime( 0 ),
    m_isCompleted(false),
    m_cselector( *this ),
    m_os( Os::get() ),
    m_paused( false )
    //,m_demoOverlay( *this )
  {
    for(int n=0; n<MT_MAX_CURSORS; n++) {
      m_createStrokes[n] = NULL;
      m_moveStrokes[n] = NULL;
      m_numRopeLength[n] = NULL;
      m_numRopeParts[n] = 0;
    }
    configureScreenTransform( m_window.width(), m_window.height() );
    m_levels = levels;
    gotoLevel(0);
  }


  virtual bool renderScene( Canvas& c, int level ) 
  {
    Scene scene( true );
    int size = m_levels->load( level, levelbuf, sizeof(levelbuf) );
    if ( size && scene.load( levelbuf, size ) ) {
      scene.draw( c, FULLSCREEN_RECT );
      return true;
    }
    return false;
  }

  void gotoLevel( int level, bool replay=false )
  {
    if ( level >= 0 && level < m_levels->numLevels() ) {
      int size = m_levels->load( level, levelbuf, sizeof(levelbuf) );
      if ( size && m_scene.load( levelbuf, size ) ) {
	m_scene.activateAll();
	//m_window.setSubName( file );
	m_refresh = true;
	if ( m_edit ) {
	  m_scene.protect(0);
	}
	m_recorder.stop();
	m_player.stop();
	if ( replay ) {
	  m_player.start( &m_recorder.getLog() );
	} else {
	  m_recorder.start();
	}
	m_level = level;
	m_stats.reset();
        if (m_levelnameOverlay && m_levelnameHideTime) {
            hideOverlay(m_levelnameOverlay);
            delete m_levelnameOverlay;
            m_levelnameHideTime = 0;
        }

        std::string title = m_scene.getTitle(), author = m_scene.getAuthor();

        /* Only show title if we have at least one of (title, author) specified */
        if (!title.empty() || !author.empty()) {
            m_levelnameOverlay = createTextOverlay( *this,
                    ((title.empty())?(std::string("Untitled")):(title)) +
                    std::string(" by ") +
                    ((author.empty())?(std::string("Anonymous")):(author)));
            m_levelnameHideTime = -1; /* in onTick, -1 means "show the overlay" */
        }
      }
    }
  }


  bool save( const char *file=NULL )
  {	  
    string p;
    if ( file ) {
      p = file;
    } else {
      p = Config::userDataDir() + Os::pathSep + "L99_saved.nph";
    }
    if ( m_scene.save( p ) ) {
      m_levels->addPath( p.c_str() );
      int l = m_levels->findLevel( p.c_str() );
      if ( l >= 0 ) {
	m_level = l;
	m_window.setSubName( p.c_str() );
      }
      return true;
    }
    return false;
  }

  bool send()
  {
    if ( save( SEND_TEMP_FILE ) ) {
      Http h;
      if ( h.post( Config::planetRoot().c_str(),
		   "upload", SEND_TEMP_FILE, "type=level" ) ) {
	std::string id = h.getHeader("NP-Upload-Id");
	if ( id.length() > 0 ) {
	  printf("uploaded as id %s\n",id.c_str());
	  if ( !m_os->openBrowser((Config::planetRoot()+"?level="+id).c_str()) ) {
	    showMessage("Unable to launch browser");
	  }
	} else {
	  showMessage("UploadFailed: unknown error");
	}
      } else {
	showMessage(std::string("UploadFailed: ")+h.errorMessage());
      }
    }
    return false;
  }

  void setTool( int t )
  {
    m_colour = t;
  }

  void editMode( bool set )
  {
    m_edit = set;
  }

  void showMessage( const std::string& msg )
  {
    //todo
    printf("showMessage \"%s\"\n",msg.c_str());
  }

  void showOverlay( Overlay* o )
  {
    parent()->add( o );
    o->onShow();
  }

  void hideOverlay( Overlay* o )
  {
    parent()->remove( o );
    o->onHide();
    m_refresh = true;
  }

  void setPause(bool pause)
  {
    if (pause == m_paused) {
      return;
    }

    if ( pause ) {
      if ( !m_pauseOverlay ) {
        m_pauseOverlay = createIconOverlay( *this, "pause.png", 50, 50 );
      }
      showOverlay( m_pauseOverlay );
    } else {
      hideOverlay( m_pauseOverlay );
    }
    m_paused = pause;
  }

  void togglePause()
  {
    setPause( !m_paused );
  }

  bool isPaused()
  {
    return m_paused;
  }

  void edit( bool doEdit )
  {
    if ( m_edit != doEdit ) {
      m_edit = doEdit;
      if ( m_edit ) {
	if ( !m_editOverlay ) {
	  m_editOverlay = createEditOverlay(*this);
	}
	showOverlay( m_editOverlay );
	m_scene.protect(0);
      } else {
	hideOverlay( m_editOverlay );
	m_strokeFixed = false;
	m_strokeSleep = false;
	m_strokeDecor = false;
	if ( m_colour < 2 ) m_colour = 2;
	m_scene.protect();
      }
    }
  }

  Vec2 cursorPoint(Uint16 x, Uint16 y)
  {
    Vec2 pt( x, y );
    worldToScreen.inverseTransform( pt );
    return pt;
  }

  Vec2 mousePoint( SDL_Event ev )
  {
    Vec2 pt( ev.button.x, ev.button.y );
    worldToScreen.inverseTransform( pt );
    return pt;
  }

  bool handleGameEvent( SDL_Event &ev )
  {
    switch( ev.type ) {
    case SDL_KEYDOWN:
      switch ( ev.key.keysym.sym ) {
      case SDLK_SPACE:
      case SDLK_KP_ENTER:
      case SDLK_RETURN:
	togglePause();
	break;
      case SDLK_s:
	save();
	break;
      case SDLK_F4: 
	showOverlay( createMenuOverlay( *this ) );
	break;
      case SDLK_c:
	showOverlay( m_cselector.overlay() );
	break;
      case SDLK_e:
      case SDLK_F6:
	edit( !m_edit );
	break;
      case SDLK_d:
	//toggleOverlay( m_demoOverlay );
	break;
      case SDLK_r:
      case SDLK_UP:
	gotoLevel( m_level );
	break;
      case SDLK_n:
      case SDLK_RIGHT:
	gotoLevel( m_level+1 );
	break;
      case SDLK_p:
      case SDLK_LEFT:
	gotoLevel( m_level-1 );
	break;
      case SDLK_v:
	gotoLevel( m_level, true );
	break;
      default:
	break;
      }
      break;
    default:
      break;
    }
    return false;
  }

  bool handleModEvent( SDL_Event &ev )
  {
    static int mod=0;
    //printf("mod=%d\n",ev.key.keysym.sym,mod);
    switch( ev.type ) {      
    case SDL_KEYDOWN:
      //printf("mod key=%x mod=%d\n",ev.key.keysym.sym,mod);
      if ( ev.key.keysym.sym == SDLK_F8 ) {
	mod = 1;  //zoom- == middle (delete)
	return true;
      } else if ( ev.key.keysym.sym == SDLK_F7 ) {
	mod = 2;  //zoom+ == right (move)
	return true;
      }
      break;
    case SDL_KEYUP:
      if ( ev.key.keysym.sym == SDLK_F7
	   || ev.key.keysym.sym == SDLK_F8 ) {
	mod = 0;     
	return true;
      }
      break;
    case SDL_MOUSEBUTTONDOWN: 
    case SDL_MOUSEBUTTONUP: 
      if ( ev.button.button == SDL_BUTTON_LEFT && mod != 0 ) {
	ev.button.button = ((mod==1) ? SDL_BUTTON_MIDDLE : SDL_BUTTON_RIGHT);
      }
      break;
    }
    return false;
  }

  bool handlePlayEvent( SDL_Event &ev )
  {
    switch( ev.type ) {      
    case SDL_MOUSEBUTTONDOWN: 
      if ( ev.button.button == SDL_BUTTON_LEFT ) {
        if ((SDL_GetModState() & KMOD_SHIFT) > 0)
        {
          if(startRope(0, ev.button.x, ev.button.y))
          {
            return true;
          }
        }
        else
        {
          if (startStroke(0, ev.button.x, ev.button.y))
          {
            return true;
          }
        }
      }
      break;
    case SDL_MOUSEBUTTONUP:
      if ( ev.button.button == SDL_BUTTON_LEFT) {
        if ((SDL_GetModState() & KMOD_SHIFT) > 0)
        {
          if(finishRope(0))
          {
            return true;
          }
        }
        else
        {
          if (finishStroke(0)) {
            return true;
          }
        }
      }
      break;
    case SDL_MOUSEMOTION:
      if ( m_createStrokes[0] ) {
        if ((SDL_GetModState() & KMOD_SHIFT) > 0)
        {
          if (extendRope(0, ev.button.x, ev.button.y))
          {
            return true;
          }
        }
        else
        {
          if (extendStroke(0, ev.button.x, ev.button.y)) {
            return true;
          }
        }
      }
      break;
    case SDL_KEYDOWN:
      if ( ev.key.keysym.sym == SDLK_ESCAPE ) {
        if ( cancelDraw(0) )
        {
          m_refresh = true;
          return true;
        }
        else if ( m_scene.deleteStroke( m_scene.strokes().at(m_scene.strokes().size()-1) ) ) {
          m_stats.undoCount++;
          m_refresh = true;
          return true;
	    }
      }
      break;
    default:
      break;
    }
    return false;
  }

  bool checkCursorId(int index)
  {
    if ((index >= MT_MAX_CURSORS) || (index < 0))
    {
      fprintf(stderr, "coursor id %d to high (max %d)\n", index, MT_MAX_CURSORS);
      return false;
    }   

    return true;
  }

  bool startStroke(int index, Uint16 x, Uint16 y)
  {
    if (!checkCursorId(index))
    {
      return false;
    }

    if (m_createStrokes[index])
    {
      fprintf(stderr, "startStroke: stroke %d already started\n", index);
      return false;
    }

    int attrib = 0;
    if ( m_strokeFixed ) {
       attrib |= ATTRIB_GROUND;
    }
    if ( m_strokeSleep ) {
       attrib |= ATTRIB_SLEEPING;
    }
    if ( m_strokeDecor ) {
       attrib |= ATTRIB_DECOR;
    }
    m_createStrokes[index] = m_scene.newStroke( Path()&cursorPoint(x, y), m_colour, attrib );
   
    return true;
  }

  bool extendStroke(int index, Uint16 x, Uint16 y)
  {
    if (!checkCursorId(index))
    {
      return false;
    }

    if ( !m_createStrokes[index] ) {
      fprintf(stderr, "extendStroke: stroke %d not yet started\n", index);
      return false;
    }
     
    m_scene.extendStroke( m_createStrokes[index], cursorPoint(x, y) );
    return true;
  }

  bool finishStroke(int index)
  {
    if (!checkCursorId(index))
    {
      return false;
    }

    if (!m_createStrokes[index]) {
      fprintf(stderr, "finishStroke: stroke %d not yet started\n", index);
      return false;
    }

    int numPoints = m_scene.numPoints(m_createStrokes[index]);
    if (numPoints < FINISH_THRESHOLD_NUM_POINTS)
    {
      fprintf(stderr, "finishStroke: can't finish stroke with %d < %d points\n", numPoints, FINISH_THRESHOLD_NUM_POINTS);
      cancelDraw(index);
      return true;
    }

    if ( m_scene.activate( m_createStrokes[index] ) ) {
      m_stats.strokeCount++;
      if ( isPaused() ) {
        m_stats.pausedStrokes++;
      }
    } else {
      m_scene.deleteStroke( m_createStrokes[index] );
    }

    fprintf(stderr, "finished stroke with %d points\n", numPoints);

    m_createStrokes[index] = NULL;

    return true;
  }

  bool startRope(int index, Uint16 x, Uint16 y)
  {
    if(!checkCursorId(index))
    {
      return false;
    }

    if (m_createStrokes[index])
    {
      fprintf(stderr, "startRope: rope %d already started\n", index);
      return false;
    }

    int attrib = 0;
    m_numRopeLength[index] = 0;
    m_numRopeParts[index] = 0;
    m_createStrokes[index] = m_scene.newStroke( Path()&cursorPoint(x, y), m_colour, attrib );
    m_ropeParts[index][0] = m_createStrokes[index];

    return true;
  }

  bool extendRope(int index, Uint16 x, Uint16 y)
  {
    if (!checkCursorId(index))
    {
      return false;
    }

    if ( !m_createStrokes[index] ) {
      fprintf(stderr, "extendRope: rope %d not yet started\n", index);
      return false;
    }
     
    m_numRopeLength[index]++;
    m_scene.extendStroke( m_createStrokes[index], cursorPoint(x, y) );

    if (m_numRopeLength[index] == 10)
    {
        nextRopePart(index, x, y);
    }
    return true;
  }

  void nextRopePart(int index, Uint16 x, Uint16 y)
  {
    if (!checkCursorId(index))
    {
      return;
    }

    if (!m_createStrokes[index]) {
      fprintf(stderr, "nextRopePart: rope %d not yet started\n", index);
      return;
    }

    if ( m_scene.activate( m_createStrokes[index] ) ) {
      m_stats.strokeCount++;
      if ( isPaused() ) {
        m_stats.pausedStrokes++;
      }
    } else {
      m_scene.deleteStroke( m_createStrokes[index] );
    }

    m_ropeParts[index][m_numRopeParts[index]++] = m_createStrokes[index];
    m_scene.setNoMass(m_createStrokes[index]);

    int attrib = 0;
    m_numRopeLength[index] = 0;
    m_createStrokes[index] = m_scene.newStroke( Path()&cursorPoint(x, y), m_colour, attrib );
  }

  bool finishRope(int index)
  {
    if (!checkCursorId(index))
    {
      return false;
    }

    if (!m_createStrokes[index]) {
      fprintf(stderr, "finishRope: rope %d not yet started\n", index);
      return false;
    }

    if ( m_scene.activate( m_createStrokes[index] ) ) {
      m_stats.strokeCount++;
      if ( isPaused() ) {
        m_stats.pausedStrokes++;
      }
    } else {
      m_scene.deleteStroke( m_createStrokes[index] );
    }
    m_createStrokes[index] = NULL;

    for(int n=0; n<m_numRopeParts[index]; n++)
    {
      m_scene.setDefaultMass(m_ropeParts[index][n]);
    }

    return true;
  }

  bool cancelDraw(int index)
  {
    if (!checkCursorId(index))
    {
      return false;
    }

    if ( !m_createStrokes[index] ) 
    {
      fprintf(stderr, "cancelDraw: line %d not yet started\n", index);
      return false;
    }

    int numPoints = m_scene.numPoints(m_createStrokes[index]);
    if ( numPoints > CANCEL_THRESHOLD_NUM_POINTS)
    {
      fprintf(stderr, "won't cancel stroke with %d > %d points\n", numPoints, CANCEL_THRESHOLD_NUM_POINTS);
      return false;
    }

    m_scene.deleteStroke( m_createStrokes[index] );
    m_createStrokes[index] = NULL;
    m_refresh = true;
    return true;
  }

  bool handleEditEvent( SDL_Event &ev )
  {
    //allow move/delete in normal play!!
    //if ( !m_edit ) return false;

    switch( ev.type ) {      
    case SDL_MOUSEBUTTONDOWN: 
      if ( ev.button.button == SDL_BUTTON_RIGHT ) {

        if (startMoveStroke(0, ev.button.x, ev.button.y)) {
          return true;
        }
      } 
      if ( ev.button.button == SDL_BUTTON_MIDDLE ) {
        if (deleteStroke(ev.button.x, ev.button.y)) {
          return true;
        }    
      }
      break;
    case SDL_MOUSEBUTTONUP:
      if ( ev.button.button == SDL_BUTTON_RIGHT ) {
        if (endMoveStroke(0)) {
          return true;
        }
      }
      break;
    case SDL_MOUSEMOTION:
      if (moveStroke(0, ev.button.x, ev.button.y)) {
        return true;
      }
      break;
    default:
      break;
    }
    return false;
  }

  bool startMoveStroke(int index, Uint16 x, Uint16 y) 
  {
    if (!checkCursorId(index))
    {
      return false;
    }

    if (m_moveStrokes[index]) {
      return false;
    }

    m_moveStrokes[index] = m_scene.strokeAtPoint( cursorPoint(x, y),
                         SELECT_TOLERANCE );

    if (m_moveStrokes[index]) {
        m_scene.setNoMass(m_moveStrokes[index]);
    }

    return true;
  }

  bool endMoveStroke(int index)
  {
    if (!checkCursorId(index))
    {
      return false;
    }

    if (!m_moveStrokes[index]) {
      return false;
    } else {
        m_scene.setDefaultMass(m_moveStrokes[index]);
    }


    m_moveStrokes[index] = NULL;
    return true;
  }

  bool moveStroke(int index, Uint16 x, Uint16 y) 
  {
    if (!checkCursorId(index))
    {
      return false;
    }

    if (!m_moveStrokes[index]) {
      return false;
    }

    m_scene.moveStroke( m_moveStrokes[index], cursorPoint(x, y) );
    return true;
  }


  bool deleteStroke(Uint16 x, Uint16 y) 
  {
    m_scene.deleteStroke( m_scene.strokeAtPoint( cursorPoint(x, y),
                         SELECT_TOLERANCE ) );
    m_refresh = true;
  }


  bool handleMultitouchEvent ( SDL_Event &ev)
  {
    switch( ev.type ) {
      case SDL_NP_START_STROKE: {
          DrawEvent*  de = (DrawEvent*)ev.user.data1;
          if (de == NULL) { return false; }

          startStroke(de->cursor_id, de->x, de->y);
          free(de);
          ev.user.data1 = NULL;
          return true;
        }

      case SDL_NP_APPEND_STROKE: {
          DrawEvent* de = (DrawEvent*)ev.user.data1;
          if (de == NULL) { return false; }

          extendStroke(de->cursor_id, de->x, de->y);
          free(de);
          ev.user.data1 = NULL;
          return true;
        }

      case SDL_NP_FINISH_STROKE: {
          DrawEvent* de = (DrawEvent*)ev.user.data1;
          if (de == NULL) { return false; }

          finishStroke(de->cursor_id);
          free(de);
          ev.user.data1 = NULL;
          return true;
        }

      case SDL_NP_START_ROPE: {
          DrawEvent*  de = (DrawEvent*)ev.user.data1;
          if (de == NULL) { return false; }

          startRope(de->cursor_id, de->x, de->y);
          free(de);
          ev.user.data1 = NULL;
          return true;
        }

      case SDL_NP_APPEND_ROPE: {
          DrawEvent*  de = (DrawEvent*)ev.user.data1;
          if (de == NULL) { return false; }

          extendRope(de->cursor_id, de->x, de->y);
          free(de);
          ev.user.data1 = NULL;
          return true;
      }

      case SDL_NP_FINISH_ROPE: {
          DrawEvent* de = (DrawEvent*)ev.user.data1;
          finishRope(de->cursor_id);
          free(de);
          return true;
        }

      case SDL_NP_CANCEL_DRAW: {
        CursorEvent* ce = (CursorEvent*)ev.user.data1;
        if (ce == NULL) { return false; }

          cancelDraw(ce->cursor_id);
          free(ce);
          ev.user.data1 = NULL;
          return true;
        }

      case SDL_NP_START_DRAG: {
          DragEvent* de = (DragEvent*)ev.user.data1;
          if (de == NULL) { return false; }

          startMoveStroke(de->cursor_id, de->x, de->y);
          free(de);
          ev.user.data1 = NULL;
          return true;
        }

      case SDL_NP_DRAG: {
          DragEvent* de = (DragEvent*)ev.user.data1;
          if (de == NULL) { return false; }

          moveStroke(de->cursor_id, de->x, de->y);
          free(de);
          ev.user.data1 = NULL;
          return true;
        }

      case SDL_NP_END_DRAG: {
          DragEvent* de = (DragEvent*)ev.user.data1;
          if (de == NULL) { return false; }

          endMoveStroke(de->cursor_id);
          free(de);
          ev.user.data1 = NULL;
          return true;
        }

      case SDL_NP_PAN:
        return true;

      case SDL_NP_ZOOM:
        return true;

      case SDL_NP_DELETE: {
          DeleteEvent* de = (DeleteEvent*)ev.user.data1;
          if (de == NULL) { return false; }

          deleteStroke(de->x, de->y);
          free(de);
          ev.user.data1 = NULL;
          return true;
        }

      case SDL_NP_PREVIEW_CURSOR: {
          DrawEvent* de = (DrawEvent*)ev.user.data1;
          if (de == NULL) { return false; }

          previewCursor(de->cursor_id, de->x, de->y);
          free(de);
          ev.user.data1 = NULL;
          return true;
        }

      case SDL_NP_RESTART_LEVEL: {
          gotoLevel( m_level );
          return true;
        }

      case SDL_NP_NEXT_LEVEL: {
          gotoLevel( m_level+1 );
          return true;
        }

      case SDL_NP_PREV_LEVEL: {
          gotoLevel( m_level-1 );
          return true;
        }

      case SDL_NP_TOGGLE_MENU: {
          toggleMenu();
          return true;
        }

      case SDL_NP_SHOW_MENU: {
          showMenu();
          return true;
        }

      case SDL_NP_HIDE_MENU: {
          hideMenu();
          return true;
        }

      case SDL_NP_TOGGLE_PAUSE: {
          togglePause();
          return true;
        }

      case SDL_NP_PAUSE: {
          setPause(true);
          return true;
        }

      case SDL_NP_RESUME: {
          setPause(false);
          return true;
        }

      case SDL_NP_SAVE: {
          save();
          return true;
        }

      case SDL_NP_TOGGLE_EDITOR: {
          edit( !m_edit );
          return true;
        }

      case SDL_NP_REPLAY: {
          gotoLevel( m_level, true );
          return true;
        }
    }

    return false;
  }

  bool previewCursor(int cursor_id, int x, int y)
  {
      m_window.previewCursor(cursor_id, x, y);
      return true;
  }

  ////////////////////////////////////////////////////////////////
  // layer interface
  ////////////////////////////////////////////////////////////////

  virtual bool isDirty()
  {
    //TODO this can be a bit heavyweight
    return !dirtyArea().isEmpty();
  }

  virtual Rect dirtyArea() 
  {
    if ( m_refresh  ) {
      m_refresh = false;
      return FULLSCREEN_RECT;
    } else {
      return m_scene.dirtyArea();
    }
  }

  virtual void onTick( int tick ) 
  {
    if ( !isPaused() ) {
      m_scene.step();
      m_recorder.tick();
      m_player.tick();
    }

    if (m_levelnameHideTime == -1 && m_levelnameOverlay) {
        showOverlay(m_levelnameOverlay);
        m_levelnameHideTime = tick + 4000;
    } else if (m_levelnameHideTime != 0 &&
            m_levelnameHideTime<tick && m_levelnameOverlay) {
        hideOverlay(m_levelnameOverlay);
        m_levelnameHideTime = 0;
    }

    SDL_Event ev;
    while ( m_player.fetchEvent(ev) ) {
      // todo only dispatch play events?
      handleModEvent(ev)
	|| handleGameEvent(ev)
	|| handleEditEvent(ev)
	|| handlePlayEvent(ev)
    || handleMultitouchEvent(ev);
    }

    if ( m_isCompleted && m_edit ) {
      hideOverlay( m_completedOverlay );
      m_isCompleted = false;
    }
    if ( m_scene.isCompleted() != m_isCompleted && !m_edit ) {
      m_isCompleted = m_scene.isCompleted();
      if ( m_isCompleted ) {
        m_stats.endTime = SDL_GetTicks();
        m_player.stop();
        m_recorder.stop();
        m_completedOverlay = createNextLevelOverlay(*this, m_scene.getWinner());
        showOverlay( m_completedOverlay );
      } else {
        hideOverlay( m_completedOverlay );
      }
    }

    if ( m_os ) {
      m_os->poll();      
      for ( char *f = m_os->getLaunchFile(); f; f=m_os->getLaunchFile() ) {
	if ( strstr(f,".npz") ) {
	  //m_levels->empty();
	}
	m_levels->addPath( f );
	int l = m_levels->findLevel( f );
	if ( l >= 0 ) {
	  gotoLevel( l );
	  m_window.raise();
	}
      }    
    }  
  }

  void toggleMenu()
  {
    if ( m_completedOverlay == NULL ) {
      showMenu();
    } else {
      hideMenu();
    }
  }

  void showMenu()
  {
    m_completedOverlay = createNextLevelOverlay(*this, -2);
    showOverlay( m_completedOverlay );
  }

  void hideMenu()
  {
    m_completedOverlay = NULL;
    hideOverlay( m_completedOverlay );
  }

  virtual void draw( Canvas& screen, const Rect& area )
  {
    m_scene.draw( screen, area );
    if ( m_fade ) {
      screen.fade( area );
    }
  }

  virtual bool handleEvent( SDL_Event& ev )
  {
    if ( m_player.isRunning() 
	 && ( ev.type==SDL_MOUSEMOTION || 
	      ev.type==SDL_MOUSEBUTTONDOWN || 
	      ev.type==SDL_MOUSEBUTTONUP ) ) {
      return false;
    } else if (handleModEvent(ev)
	       || handleGameEvent(ev)
	       || handleEditEvent(ev)
	       || handlePlayEvent(ev)
           || handleMultitouchEvent(ev)) {
      //\TODO only record edit,play events etc
      m_recorder.record( ev );
      return true;
    }
    return false;
  }

  virtual Window* getWindow() {
      return &m_window;
  }
};



Widget* createGameLayer( Levels* levels, int width, int height, bool fullscreen )
{
  return new Game(levels,width,height,fullscreen);
}
