/*
 * This file is part of NumptyPhysics
 * Copyright (C) 2008 Tim Edmonds
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 */
#include "Common.h"
#include "Array.h"
#include "Config.h"
#include "Scene.h"

#include <sstream>
#include <fstream>


Transform::Transform( float32 scale, float32 rotation, const Vec2& translation )
{
  set( scale, rotation, translation );
}

void Transform::set( float32 scale, float32 rotation, const Vec2& translation )
{
  if ( scale==0.0f && rotation==0.0f && translation==Vec2(0,0) ) {
    m_bypass = true;
  } else {
    m_rot.Set( rotation );
    m_pos = translation;
    m_rot.col1.x *= scale;
    m_rot.col1.y *= scale;
    m_rot.col2.x *= scale;
    m_rot.col2.y *= scale;
    m_invrot = m_rot.Invert();
    m_bypass = false;
  }
}

Transform worldToScreen( 0.5f, M_PI/2, Vec2(240,0) );

void configureScreenTransform( int w, int h )
{
  SCREEN_WIDTH = w;
  SCREEN_HEIGHT = h;
  FULLSCREEN_RECT = Rect(0,0,w-1,h-1);
  if ( w==WORLD_WIDTH && h==WORLD_HEIGHT ) { //unity
    worldToScreen.set( 0.0f, 0.0f, Vec2(0,0) );
  } else {
    float rot = 0.0f;
    Vec2 tr(0,0);
    if ( h > w ) { //portrait
      rot = M_PI/2;
      tr = Vec2( w, 0 );
      b2Swap( h, w );
    }
    float scalew = (float)w/(float)WORLD_WIDTH;
    float scaleh = (float)h/(float)WORLD_HEIGHT;
    if ( scalew < scaleh ) {
      worldToScreen.set( scalew, rot, tr );
    } else {
      worldToScreen.set( scaleh, rot, tr );
    }
  }
}


class Stroke
{
public:

private:
  struct JointDef : public b2RevoluteJointDef
  {
    JointDef( b2Body* b1, b2Body* b2, const b2Vec2& pt )
    {
      Initialize( b1, b2, pt );
      maxMotorTorque = 10.0f;
      motorSpeed = 0.0f;
      enableMotor = true;
    }
  };

  struct BoxDef : public b2PolygonDef
  {
    void init( const Vec2& p1, const Vec2& p2, int attr )
    {
      b2Vec2 barOrigin = p1;
      b2Vec2 bar = p2 - p1;
      bar *= 1.0f/PIXELS_PER_METREf;
      barOrigin *= 1.0f/PIXELS_PER_METREf;;
      SetAsBox( bar.Length()/2.0f, 0.1f,
		0.5f*bar + barOrigin, vec2Angle( bar ));
      //      SetAsBox( bar.Length()/2.0f+b2_toiSlop, b2_toiSlop*2.0f,
      //	0.5f*bar + barOrigin, vec2Angle( bar ));
      friction = 0.3f;
      if ( attr & ATTRIB_GROUND ) {
	density = 0.0f;
      } else if ( attr & ATTRIB_GOAL ) {
	density = 100.0f;
      } else if ( attr & ATTRIB_TOKEN ) {
	density = 3.0f;
	friction = 0.1f;
      } else {
	density = 5.0f;
      }
      restitution = 0.2f;
    }
  };

public:
  Stroke( const Path& path, int attributes )
    : m_rawPath(path)
  {
    m_colour = brushColours[DEFAULT_BRUSH];
    m_attributes = attributes;
    m_origin = m_rawPath.point(0);
    m_rawPath.translate( -m_origin );
    setPlayer(0);
    reset();
  }  

  Stroke( const std::string& str ) 
  {
    setPlayer(0);
    int col = 0;
    m_colour = brushColours[DEFAULT_BRUSH];
    m_attributes = 0;
    m_origin = Vec2(400,240);
    reset();
    const char *s = str.c_str();
    while ( *s && *s!=':' && *s!='\n' ) {
      switch ( *s ) {
      case 't': setAttribute( ATTRIB_TOKEN ); setPlayer(0); break;	
      case 'T': setAttribute( ATTRIB_TOKEN ); setPlayer(1); break;	
      case 'g': setAttribute( ATTRIB_GOAL ); setPlayer(0); break;	
      case 'G': setAttribute( ATTRIB_GOAL ); setPlayer(1); break;	
      case 'f': setAttribute( ATTRIB_GROUND ); break;
      case 's': setAttribute( ATTRIB_SLEEPING ); break;
      case 'd': setAttribute( ATTRIB_DECOR ); break;
      default:
	if ( *s >= '0' && *s <= '9' ) {
	  col = col*10 + *s -'0';
	}
	break;
      }
      s++;
    }
    if ( col >= 0 && col < NUM_BRUSHES ) {
      m_colour = brushColours[col];
    }
    if ( *s++ == ':' ) {
      m_rawPath = Path(s);
    }
    if ( m_rawPath.size() < 2 ) {
      throw "invalid stroke def";
    }
    m_origin = m_rawPath.point(0);
    m_rawPath.translate( -m_origin );
    setAttribute( ATTRIB_DUMMY );
  }

  void reset( b2World* world=NULL )
  {
    if (m_body && world) {
      world->DestroyBody( m_body );
    }
    m_body = NULL;
    m_xformAngle = 7.0f;
    m_drawnBbox.tl = m_origin;
    m_drawnBbox.br = m_origin;
    m_jointed[0] = m_jointed[1] = false;
    m_shapePath = m_rawPath;
    m_hide = 0;
    m_drawn = false;
  }

  std::string asString()
  {
    std::stringstream s;
    s << 'S';
    if ( hasAttribute(ATTRIB_TOKEN) )    s<<'t';
    if ( hasAttribute(ATTRIB_GOAL) )     s<<'g';
    if ( hasAttribute(ATTRIB_GROUND) )   s<<'f';
    if ( hasAttribute(ATTRIB_SLEEPING) ) s<<'s';
    if ( hasAttribute(ATTRIB_DECOR) )    s<<'d';
    for ( int i=0; i<NUM_BRUSHES; i++ ) {
      if ( m_colour==brushColours[i] )  s<<i;
    }
    s << ":";
    transform();
    for ( int i=0; i<m_xformedPath.size(); i++ ) {
      const Vec2& p = m_xformedPath.point(i);
      s <<' '<< p.x << ',' << p.y; 
    }
    s << std::endl;
    return s.str();
  }

  void setAttribute( Attribute a )
  {
    m_attributes |= a;
    if ( m_attributes & ATTRIB_TOKEN )     m_colour = brushColours[RED_BRUSH];
    else if ( m_attributes & ATTRIB_GOAL ) m_colour = brushColours[YELLOW_BRUSH];
  }

  void setPlayer( int player )
  {
      m_player = player;
  }

  int getPlayer() 
  {
      return m_player;
  }

  void clearAttribute( Attribute a )
  {
    m_attributes &= ~a;
  }

  bool hasAttribute( Attribute a )
  {
    return (m_attributes&a) != 0;
  }
  void setColour( int c ) 
  {
    m_colour = c;
  }

  void createBodies( b2World& world )
  {
    process();
    if ( hasAttribute( ATTRIB_DECOR ) ){
      return; //decorators have no physical embodiment
    }
    int n = m_shapePath.numPoints();
    if ( n > 1 ) {
      b2BodyDef bodyDef;
      bodyDef.position = m_origin;
      bodyDef.position *= 1.0f/PIXELS_PER_METREf;
      bodyDef.userData = this;
      if ( m_attributes & ATTRIB_SLEEPING ) {
	bodyDef.isSleeping = true;
      }
      m_body = world.CreateBody( &bodyDef );
      for ( int i=1; i<n; i++ ) {
	BoxDef boxDef;
	boxDef.init( m_shapePath.point(i-1),
		     m_shapePath.point(i),
		     m_attributes );
	m_body->CreateShape( &boxDef );
      }
      m_body->SetMassFromShapes();

    }
    transform();
  }

  void setNoMass()
  {
      static b2MassData m;
      if (m_body) {
          m_body->SetMass(&m);
      }
  }

  void setDefaultMass()
  {
      if (m_body) {
          m_body->SetMassFromShapes();
      }
  }

  bool maybeCreateJoint( b2World& world, Stroke* other )
  {
    if ( (m_attributes&ATTRIB_CLASSBITS)
	 != (other->m_attributes&ATTRIB_CLASSBITS) ) {
      return false; // can only joint matching classes
    } else if ( hasAttribute(ATTRIB_GROUND) ) {
      return true; // no point jointing grounds
    } else if ( m_body && other->body() ) {
      transform();
      int n = m_xformedPath.numPoints();
      for ( int end=0; end<2; end++ ) {
	if ( !m_jointed[end] ) {
	  const Vec2& p = m_xformedPath.point( end ? n-1 : 0 );
	  if ( other->distanceTo( p ) <= JOINT_TOLERANCE ) {
	    //printf("jointed end %d d=%f\n",end,other->distanceTo( p ));
	    b2Vec2 pw = p;
	    pw *= 1.0f/PIXELS_PER_METREf;
	    JointDef j( m_body, other->m_body, pw );
	    world.CreateJoint( &j );
	    m_jointed[end] = true;
	  }
	}
      }
    }
    if ( m_body ) {
      return m_jointed[0] && m_jointed[1];
    }
    return true; ///nothing to do
  }

  void draw( Canvas& canvas )
  {
    if ( m_hide < HIDE_STEPS ) {
      bool thick = (canvas.width() > 400);
      transform();
      canvas.drawPath( m_screenPath, canvas.makeColour(m_colour), thick );
      m_drawn = true;
    }
    m_drawnBbox = m_screenBbox;
  }

  void addPoint( const Vec2& pp ) 
  {
    Vec2 p = pp; p -= m_origin;
    Vec2 p2 = m_rawPath.point( m_rawPath.numPoints()-1 );

    /* avoiding assertions in b2PolygonShape.cpp when using MT input */
    int xdiff = p.x - p2.x;
    int ydiff = p.y - p2.y;
    double h = xdiff * xdiff + ydiff * ydiff;

    if (h < B2_FLT_EPSILON * B2_FLT_EPSILON)
    {
      fprintf(stderr, "ignoring nearby points\n");
      return;
    }

    if ( p == m_rawPath.point( m_rawPath.numPoints()-1 ) ) {
    } else {
      m_rawPath.append( p );
      m_drawn = false;
    }
  }

  void origin( const Vec2& p ) 
  {
    // todo 
    if ( m_body ) {
      b2Vec2 pw = p;
      pw *= 1.0f/PIXELS_PER_METREf;
      m_body->SetXForm( pw, m_body->GetAngle() );
    }
    m_origin = p;
  }

  b2Body* body() { return m_body; }

  float32 distanceTo( const Vec2& pt )
  {
    float32 best = 100000.0;
    transform();
    for ( int i=1; i<m_xformedPath.numPoints(); i++ ) {    
      Segment s( m_xformedPath.point(i-1), m_xformedPath.point(i) );
      float32 d = s.distanceTo( pt );
      //printf("  d[%d]=%f %d,%d\n",i,d,m_rawPath.point(i-1).x,m_rawPath.point(i-1).y);
      if ( d < best ) {
        best = d;
      }
    }
    return best;
  }

  Rect screenBbox() 
  {
    transform();
    return m_screenBbox;
  }

  Rect lastDrawnBbox() 
  {
    return m_drawnBbox;
  }

  Rect worldBbox() 
  {
    return m_xformedPath.bbox();
  }

  bool isDirty()
  {
    return (!m_drawn || transform()) && !hasAttribute(ATTRIB_DELETED);
  }

  void hide()
  {
    if ( m_hide==0 ) {
      m_hide = 1;
      
      if (m_body) {
	// stash the body where no-one will find it
	m_body->SetXForm( b2Vec2(0.0f,SCREEN_HEIGHT*2.0f), 0.0f );
	m_body->SetLinearVelocity( b2Vec2(0.0f,0.0f) );
	m_body->SetAngularVelocity( 0.0f );
      }
    }
  }

  bool hidden()
  {
    return m_hide >= HIDE_STEPS;
  }

  int numPoints()
  {
    return m_rawPath.numPoints();
  }

private:
  static float32 vec2Angle( b2Vec2 v ) 
  {
    return b2Atan2(v.y, v.x);
  } 

  void process()
  {
    float32 thresh = SIMPLIFY_THRESHOLDf;
    m_rawPath.simplify( thresh );
    m_shapePath = m_rawPath;

    while ( m_shapePath.numPoints() > MULTI_VERTEX_LIMIT ) {
      thresh += SIMPLIFY_THRESHOLDf;
      m_shapePath.simplify( thresh );
    }
  }

  bool transform()
  {
    // distinguish between xformed raw and shape path as needed
    if ( m_hide ) {
      if ( m_hide < HIDE_STEPS ) {
	//printf("hide %d\n",m_hide);
	Vec2 o = m_screenBbox.centroid();
	m_screenPath -= o;
	m_screenPath.scale( 0.99 );
	m_screenPath += o;
	m_screenBbox = m_screenPath.bbox();      
	m_hide++;
	return true;
      }
    } else if ( m_body ) {
      if ( hasAttribute( ATTRIB_DECOR ) ) {
	return false; // decor never moves
      } else if ( hasAttribute( ATTRIB_GROUND )	   
		  && m_xformAngle == m_body->GetAngle() ) {
	return false; // ground strokes never move.
      } else if ( m_xformAngle != m_body->GetAngle() 
	   ||  ! (m_xformPos == m_body->GetPosition()) ) {
	//printf("transform stroke - rot or pos\n");
	b2Mat22 rot( m_body->GetAngle() );
	b2Vec2 orig = PIXELS_PER_METREf * m_body->GetPosition();
	m_xformedPath = m_rawPath;
	m_xformedPath.rotate( rot );
	m_xformedPath.translate( Vec2(orig) );
	m_xformAngle = m_body->GetAngle();
	m_xformPos = m_body->GetPosition();
	worldToScreen.transform( m_xformedPath, m_screenPath );
	m_screenBbox = m_screenPath.bbox();      
      } else {
	//printf("transform none\n");
	return false;
      }
    } else {
      //printf("transform no body\n");
      m_xformedPath = m_rawPath;
      m_xformedPath.translate( m_origin );
      worldToScreen.transform( m_xformedPath, m_screenPath );
      m_screenBbox = m_screenPath.bbox();      
      return false;
    }
    return true;
  }

  Path      m_rawPath;
  int       m_colour;
  int       m_attributes;
  int       m_player;
  Vec2      m_origin;
  Path      m_shapePath;
  Path      m_xformedPath;
  Path      m_screenPath;
  float32   m_xformAngle;
  b2Vec2    m_xformPos;
  Rect      m_screenBbox;
  Rect      m_drawnBbox;
  bool      m_drawn;
  b2Body*   m_body;
  bool      m_jointed[2];
  int       m_hide;
};

Scene::Scene( bool noWorld )
  : m_world( NULL ),
    m_bgImage( NULL ),
    m_protect( 0 )
{
  if ( !noWorld ) {
    b2AABB worldAABB;
    worldAABB.lowerBound.Set(-100.0f, -100.0f);
    worldAABB.upperBound.Set(100.0f, 100.0f);
    
    b2Vec2 gravity(0.0f, 10.0f*PIXELS_PER_METREf/GRAVITY_FUDGEf);
    bool doSleep = true;
    m_world = new b2World(worldAABB, gravity, doSleep);
    m_world->SetContactListener( this );
  }
}

Scene::~Scene()
{
  clear();
  if ( m_world ) {
    step();
    delete m_world;
  }
}

Stroke* Scene::newStroke( const Path& p, int colour, int attribs ) {
  Stroke *s = new Stroke(p, attribs);
  switch ( colour ) {
  case 0: s->setAttribute( ATTRIB_TOKEN ); break;
  case 1: s->setAttribute( ATTRIB_GOAL ); break;
  default: s->setColour( brushColours[colour] ); break;
  }
  s->setPlayer(0);
  m_strokes.append( s );
  return s;
}

bool Scene::deleteStroke( Stroke *s ) {
  if ( s ) {
    int i = m_strokes.indexOf(s);
    if ( i >= m_protect ) {
	reset(s);
	m_strokes.erase( m_strokes.indexOf(s) );
	return true;
    }
  }
  return false;
}


void Scene::extendStroke( Stroke* s, const Vec2& pt )
{
  if ( s ) {
    s->addPoint( pt );
  }
}

void Scene::moveStroke( Stroke* s, const Vec2& origin )
{
  if ( s ) {
    int i = m_strokes.indexOf(s);
    if ( i >= m_protect ) {
      s->origin( origin );
    }
  }
}
	
void Scene::setNoMass(Stroke* s)
{
    s->setNoMass();
}

void Scene::setDefaultMass(Stroke* s)
{
    s->setDefaultMass();
}

int Scene::numPoints(Stroke* s)
{
    return s->numPoints();
}

bool Scene::activate( Stroke *s )
{
  if ( s->numPoints() > 1 ) {
    s->createBodies( *m_world );
    createJoints( s );
    return true;
  }
  return false;
}

void Scene::activateAll()
{
  m_num_goal = 0;
  m_num_token = 0;
  m_num_player = 0;
  m_winner = -1;

  for ( int i=0; i < m_strokes.size(); i++ ) {
    m_strokes[i]->createBodies( *m_world );
    if (m_strokes[i]->hasAttribute(ATTRIB_TOKEN)) { m_num_token++;}
    if (m_strokes[i]->hasAttribute(ATTRIB_GOAL)) { m_num_goal++; }
    if (m_strokes[i]->getPlayer() + 1 > m_num_player) { m_num_player = m_strokes[i]->getPlayer() + 1; }
  }
  for ( int i=0; i < m_strokes.size(); i++ ) {
    createJoints( m_strokes[i] );
  }
}

void Scene::createJoints( Stroke *s )
{
  for ( int j=m_strokes.size()-1; j>=0; j-- ) {      
    if ( s != m_strokes[j] ) {
	//printf("try join to %d\n",j);
	s->maybeCreateJoint( *m_world, m_strokes[j] );
	m_strokes[j]->maybeCreateJoint( *m_world, s );
    }
  }    
}

void Scene::step()
{
  m_world->Step( ITERATION_TIMESTEPf, SOLVER_ITERATIONS );
  // clean up delete strokes
  for ( int i=0; i< m_strokes.size(); i++ ) {
    if ( m_strokes[i]->hasAttribute(ATTRIB_DELETED) ) {
	m_strokes[i]->clearAttribute(ATTRIB_DELETED);
	m_strokes[i]->hide();
    }	   
  }
  // check for token respawn
  for ( int i=0; i < m_strokes.size(); i++ ) {
    if ( m_strokes[i]->hasAttribute( ATTRIB_TOKEN )
	   && !BOUNDS_RECT.intersects( m_strokes[i]->worldBbox() ) ) {
	reset( m_strokes[i] );
	activate( m_strokes[i] );	  
    }
  }
}

// b2ContactListener callback when a new contact is detected
void Scene::Add(const b2ContactPoint* point) 
{     
  // check for completion
  //if (c->GetManifoldCount() > 0) {
  Stroke* s1 = (Stroke*)point->shape1->GetBody()->GetUserData();
  Stroke* s2 = (Stroke*)point->shape2->GetBody()->GetUserData();

  if ( s1 && s2 ) {
    if ( s2->hasAttribute(ATTRIB_TOKEN) ) {
	b2Swap( s1, s2 );
    }
    if ( s1->hasAttribute(ATTRIB_TOKEN) 
	   && s2->hasAttribute(ATTRIB_GOAL) ) {
	  s2->setAttribute(ATTRIB_DELETED);
      if (m_num_goal == 1) {
        m_winner = s1->getPlayer();
      }
    }
  }
}

bool Scene::isCompleted()
{
  /* if we only have one goal, m_winner has already been set */
  if ((m_num_goal == 1) && (m_winner != -1)) {
    return true;
  }
  
  else if (m_num_goal > 1)
  {
   int left[m_num_player];
   for(int n=0; n<m_num_player; n++) {
     left[n] = 0;
   }

   for ( int i=0; i < m_strokes.size(); i++ ) {
     if ( m_strokes[i]->hasAttribute( ATTRIB_GOAL )
	   && !m_strokes[i]->hidden() ) {
       left[m_strokes[i]->getPlayer()]++;
      }
    }

    for ( int n=0; n<m_num_player; n++) {
      if (left[n] == 0) {
        m_winner = n;
        return true;
      }
    }
  }

  return false;
}

int Scene::getWinner()
{
  return m_num_player == 1 ? -1 : m_winner;
}

Rect Scene::dirtyArea()
{
  Rect r(0,0,0,0),temp;
  int numDirty = 0;
  for ( int i=0; i<m_strokes.size(); i++ ) {
    if ( m_strokes[i]->isDirty() ) {
	// acumulate new areas to draw
	temp = m_strokes[i]->screenBbox();
	if ( !temp.isEmpty() ) {
	  if ( numDirty==0 ) {	
	    r = temp;
	  } else {
	    r.expand( m_strokes[i]->screenBbox() );
	  }
	  // plus prev areas to erase
	  r.expand( m_strokes[i]->lastDrawnBbox() );
	  numDirty++;
	}
    }
  }
  if ( !r.isEmpty() ) {
    // expand to allow for thick lines
    r.tl.x--; r.tl.y--;
    r.br.x++; r.br.y++;
  }
  return r;
}

void Scene::draw( Canvas& canvas, const Rect& area )
{
  if ( m_bgImage ) {
    canvas.setBackground( m_bgImage );
  } else {
    canvas.setBackground( 0 );
  }
  canvas.clear( area );
  Rect clipArea = area;
  clipArea.tl.x--;
  clipArea.tl.y--;
  clipArea.br.x++;
  clipArea.br.y++;
  for ( int i=0; i<m_strokes.size(); i++ ) {
    if ( area.intersects( m_strokes[i]->screenBbox() ) ) {
	m_strokes[i]->draw( canvas );
    }
  }
  //canvas.drawRect( area, 0xffff0000, false );
}

void Scene::reset( Stroke* s )
{
  for ( int i=0; i<m_strokes.size(); i++ ) {
    if (s==NULL || s==m_strokes[i]) {
	m_strokes[i]->reset(m_world);
    }
  }    
}

Stroke* Scene::strokeAtPoint( const Vec2 pt, float32 max )
{
  Stroke* best = NULL;
  for ( int i=0; i<m_strokes.size(); i++ ) {
    float32 d = m_strokes[i]->distanceTo( pt );
    //printf("stroke %d dist %f\n",i,d);
    if ( d < max ) {
	max = d;
	best = m_strokes[i];
    }
  }
  return best;
}

void Scene::clear()
{
  reset();
  while ( m_strokes.size() ) {
    delete m_strokes[0];
    m_strokes.erase(0);
  }
  if ( m_world ) {
    //step is required to actually destroy bodies and joints
    m_world->Step( ITERATION_TIMESTEPf, SOLVER_ITERATIONS );
  }
}

void Scene::setGravity( const std::string& s )
{
  float32 x,y;      
  if ( sscanf( s.c_str(), "%f,%f", &x, &y )==2) {
    if ( m_world ) {
	b2Vec2 g(x,y);
	g *= PIXELS_PER_METREf/GRAVITY_FUDGEf;
	m_world->SetGravity( g );
    }
  } else {
    fprintf(stderr,"invalid gravity vector\n");
  }
}

bool Scene::load( unsigned char *buf, int bufsize )
{  
  std::string s( (const char*)buf, bufsize );
  std::stringstream in( s, std::ios::in );
  return load( in );
}

bool Scene::load( const std::string& file )
{
  std::ifstream in( file.c_str(), std::ios::in );
  return load( in ); 
}

bool Scene::load( std::istream& in )
{
  clear();
  if ( g_bgImage==NULL ) {
    g_bgImage = new Image("paper.png");
    g_bgImage->scale( SCREEN_WIDTH, SCREEN_HEIGHT );
  }
  m_bgImage = g_bgImage;
  std::string line;
  while ( !in.eof() ) {
    getline( in, line );
    parseLine( line );
  }
  protect();
  return true;
}

void Scene::trimWhitespace( std::string& s )
{
  static const char* whitespace = " \t";
  size_t start = s.find_first_not_of(whitespace);
  size_t end = s.find_last_not_of(whitespace);

  if (std::string::npos == start || std::string::npos == end) {
      s = "";
  } else {
      s = s.substr(start, end-start+1);
  }
}

bool Scene::parseLine( const std::string& line )
{
  try {
    switch( line[0] ) {
    case 'T': m_title = line.substr(line.find(':')+1);
              trimWhitespace(m_title); return true;
    case 'B': m_bg = line.substr(line.find(':')+1);     return true;
    case 'A': m_author = line.substr(line.find(':')+1);
              trimWhitespace(m_author);
              /* The original levels have their author set
               * to "test". Remove the author in that case. */
              if (m_author.compare("test") == 0) {
                  m_author = "";
              } return true;
    case 'S': {
                  Stroke *s = new Stroke(line); 
                  m_strokes.append(s); 
                  return true;
              }
    case 'G': setGravity(line.substr(line.find(':')+1));return true;
    }
  } catch ( const char* e ) {
    printf("Stroke error: %s\n",e);
  }
  return false;
}

void Scene::protect( int n )
{
  m_protect = (n==-1 ? m_strokes.size() : n );
}

bool Scene::save( const std::string& file )
{
  printf("saving to %s\n",file.c_str());
  std::ofstream o( file.c_str(), std::ios::out );
  if ( o.is_open() ) {
    o << "Title: "<<m_title<<std::endl;
    o << "Author: "<<m_author<<std::endl;
    o << "Background: "<<m_bg<<std::endl;
    for ( int i=0; i<m_strokes.size(); i++ ) {
	o << m_strokes[i]->asString();
    }
    o.close();
    return true;
  } else {
    return false;
  }
}


Image *Scene::g_bgImage = NULL;

